# The most pythonic AVL-Tree

An implementation of the amazing [AVL-Tree](https://en.wikipedia.org/wiki/AVL_tree).

Implementation of the thing goes into `avl.py`. To run tests on it, do `./main.py` in your terminal.
