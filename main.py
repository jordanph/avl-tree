#!/usr/bin/env python3

from avl import AvlTree

def run_tests():
    s = {2, 3, 5, 7}
    t = AvlTree(s)
    for i in s:
        if not t.contains(i):
            return False

    s1 = {1, 2, 3, 4}
    for i in s1:
        t.insert(i)
        if not t.contains(i):
            return False

    s2 = {3, 5, 6, 7}
    for i in s2:
        t.remove(i)
        if t.contains(i):
            return False

    return True


if __name__ == '__main__':
    print('Running some tests...')

    if run_tests():
        print('Hurray, it might work!')
    else:
        print("Oops, seems like something's not quite working yet.")
