# Desclaimer 1: I did as much as it did not tickle my brain cells. If you insiste in me finishing this properly then I will do it, but I am personally not into it.
# Desclaimer 2: I hope I got the main ideas and concpets correctly. Please provide some feedback.
# Questions I could ask Google but why not try Philip:
#                                                    Is this self thing necessary and should I use it in the body of a function
#                                                    Is Python call by value or call by reference? If I change something in the body of a methode, will it change
#                                                    every where or do I have to assign it as well? (Desclaimer: I still have problems with this in java as well.)
#                                                    What is up with private and public? Can I access all variables of an object just by calling it? Are getters                                                   and setters required?
#                                                    I find it weird that there are no type spicifications. How does that work here.
#                                                    What exactly should I initilize.

# a general abstract set, just to show how inheritance is done
class TreeSet:
    # insert x into the set
    def insert(self, x):
        pass

    # return True/False depending on whether x is contained
    def contains(self, x):
        pass

    # remove x if present, return True if it was there, False else
    def remove(self, x):
        pass


# should be used as nodes to construct tree from
class Node:
    def __init__(self, key):
        self.key = key
        self.left = self.right = None
#       self.hight = 0

    # Getters and setters since someone has requested clean code!
    def getKey(self):
        return self.key

    def getRight(self):
        return self.right

    def getLeft(self):
        return self.left

#    def getHeight(self):
#        return self.height

    def setLeft(self, left):
        self.left = left

    def setRight(self, right):
        self.right = right

#    def setHeight(self, hight):
#        self.hight = hight

# TreeSet implementation enabling all operations to run in O(log n) time
class AvlTree(TreeSet):
    def __init__(self, init_content=set()):
        # TODO initilization goes here

        # insert set of initial elements
        for i in init_content:
            self.insert(i)

    def insert(self, x):
        # To aviod duplicates
        if self.contains(x) == False:
            #!!!!!!!!!!!!!!! position_in_tree should be the root!!!!!!!!!!!!!
            parent = self.scan(x, position_in_tree)
            # Insert to the left
            if x.getKey() > parent.getKey():
                parent.setLeft(x)
            # Insert to the right
            else:
                parent.setRight(x)
            rebalance()

    def contains(self, x):
        # Find the potential position of x
        #!!!!!!!!!!!!!!! position_in_tree should be the root!!!!!!!!!!!!!
        parent = scan(position_in_tree, x)
        if (parent.getLeft() != None and parent.getLeft().getKey() == x.getKey()) or (parent.getRight() != None and parent.getRight().getKey() == x.getKey()):
            return True
        else:
            return False

    def remove(self, x):
        if self.contains(x) == True:
            #!!!!!!!!!!!!!!! position_in_tree should be the root!!!!!!!!!!!!!
            parent = self.scan(x, position_in_tree)
            # Insert to the left
            if x.getKey() > parent.getKey():
                parent.setLeft(None)
            # Insert to the right
            else:
                parent.setRight(None)
            rebalance()
            return True
        else:
            return False

    # Returns the new root after the right rotation
    def rightRotation(self, unbalanced_node):
        temp = unbalanced_node.getLeft()
        unbalanced_node.setLeft(temp.getRight)
        temp.setRight(unbalanced_node)
        return temp

    # Returns the new root after the left rotation
    def leftRotation(self, unbalanced_node):
        temp = unbalanced_node.getRight()
        unbalanced_node.setLeft(temp.getLeft)
        temp.setLeft(unbalanced_node)
        return temp

    # Determines the height difference between the left and the right branch
    def balanceCheck(self, parent):
        return self.leftBalance(parent) - self.rightBalance(parent)

    # Determines the length of the right branch
    def rightBalance(self, parent):
        right_balance = 0
        while parent.getRight() != None
            right_balance++
            parent = parent.getRight()
        return right_balance

    # Determines the length of the left branch
    def leftBalance(self, parent):
        left_balance = 0
        while parent.getLeft() != None
            left_balance++
            parent = parent.getRight()
        return left_balance

    # Scans the tree for the parent node of the variable conatined in the parameter node
    def scan(self, position_in_tree, node):
        # Go to left
        if node.getKey() > position_in_tree.getKey() and position_in_tree.getLeft() != None:
            scan(position_in_tree.getLeft(), node)
        # Go to right
        elif node.getKey() < position_in_tree.getKey() and position_in_tree.getRight() != None:
            scan(position_in_tree.getRight(), node)
        else:
            return position_in_tree

    def rebalance(self):
        pass
